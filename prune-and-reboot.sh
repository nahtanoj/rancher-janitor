#!/bin/bash

echo "Starting janitor prune"
docker system prune -af --volumes

if [ "${HOST_REBOOT}" == "true" ]; then
  echo "Rebooting host"
  echo b > /sysrq-trigger
fi
