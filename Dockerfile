FROM alpine:3.6

ARG DOCKER_VERSION=17.05.0-ce

ENV CROND_TEMPLATE "0 3 * * *"
ENV HOST_REBOOT "false"
ENV EXTRA_COMMAND ""
ENV EXTRA_COMMAND_CROND_TEMPLATE ""

RUN apk -v --update add bash curl && rm /var/cache/apk/*

RUN curl https://get.docker.com/builds/Linux/x86_64/docker-${DOCKER_VERSION}.tgz | tar -xz --strip 1 -C /usr/local/bin/ && chmod +x /usr/local/bin/docker

ADD start-janitor.sh /usr/local/bin
ADD prune-and-reboot.sh /usr/local/bin

CMD /usr/local/bin/start-janitor.sh
