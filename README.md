![janitor-banner](/uploads/1cf513f847e6d3099cd44bc3d6afb1e9/janitor-banner.png)

This image can clean rancher host, removing exited containers, unused images, and dangling volumes.

Limitations:
* if using `HOST_REBOOT` option, you must share `/proc/sysrq-trigger` with the container

You must use the global service type for `janitor` to run on every host.

Use the environment variable CROND_TEMPLATE to specify the crond rule. The default rule is `0 2 * * *` wich start the `janitor` once a day at 2am.

Extra command: use `EXTRA_COMMAND` and `EXTRA_COMMAND_CROND_TEMPLATE` to define extra commands.


```yaml
version: '2'
services:
  janitor:
    image: registry.gitlab.com/recalbox/ops/rancher-janitor:1.0.0
    environment:
      EXTRA_COMMAND: "ls *"
      EXTRA_COMMAND_CROND_TEMPLATE: 00 01 * * *
      CROND_TEMPLATE: 00 01 * * *
      HOST_REBOOT: 'true'
    volumes:
    - /var/run/docker.sock:/var/run/docker.sock
    - /proc/sysrq-trigger:/sysrq-trigger
    logging:
      driver: json-file
      options:
        max-file: '1'
        max-size: 50m
    labels:
      io.rancher.container.hostname_override: container_name
      io.rancher.container.pull_image: always
      io.rancher.scheduler.global: 'true'
```
