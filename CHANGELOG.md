# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.4.0] - 2018-05-23
### Changed
- add "-a --volumes" options to docker system prune command

## [1.3.0] - 2018-04-20
### added
- extra command options

## [1.2.0] - 2018-04-11
### added
- using docker system prune command to clean host

## [1.1.1] - 2017-07-17
### fixed
- remove force parameter of docker rmi

## [1.1.0] - 2017-07-17
### Added
- HOST_REBOOT support


## [1.0.0] - 2017-07-16
### Added
- host cleaning with crond rule.
